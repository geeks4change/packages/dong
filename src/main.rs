use std::{env, io};
use std::process::ExitCode;

pub const PROCESS_FAILURE: u8 = 255;
pub const PROCESS_TERMINATED: u8 = 254;
pub const PROCESS_INVALID_EXIT_CODE: u8 = 253;
pub const PROCESS_MAX_VALID_EXIT_CODE: i32 = 252;

fn main() -> ExitCode {
    let args: Vec<String> = env::args().collect();

    if args.len() < 2  {
        eprintln!("Usage:\n\ndong [command and parameters]\n\nMakes ding on success and dong on failure.");
        return ExitCode::FAILURE;
    }

    let program = &args[1];
    let args = &args[2..];
    let maybe_success = execute(program, args);
    match maybe_success {
        Ok(exit_code) => {
            play_sound(exit_code == 0);
            ExitCode::from(exit_code)
        }
        Err(error) => {
            eprintln!("Error: {}", error.to_string());
            ExitCode::from(PROCESS_FAILURE)
        }
    }
}

fn execute(program: &String, args: &[String]) -> Result<u8, io::Error> {
    use std::process::{Command, Stdio};

    let mut command = Command::new(program);
    let command = command.args(args);
    let command = command.stdin(Stdio::inherit())
        .stdout(Stdio::inherit()).stderr(Stdio::inherit());
    let status_result = command.status();
    let success = match status_result {
        Err(error) => Err(error),
        Ok(status) => match status.code() {
            Some(exit_code_i32) => Ok(if exit_code_i32 > PROCESS_MAX_VALID_EXIT_CODE { PROCESS_INVALID_EXIT_CODE } else {exit_code_i32.try_into().unwrap()}),
            None => Ok(PROCESS_TERMINATED),
        }
    };
    success
}

fn play_sound(success: bool) {
    use rodio::{Decoder, OutputStream, Sink};

    let (_stream, stream_handle) = OutputStream::try_default().unwrap();
    let sink = Sink::try_new(&stream_handle).unwrap();

    // Muscle this, as different size arrays have different expression types.
    if  success {
        let sound = std::io::Cursor::new(include_bytes!("../sounds/ding.mp3"));
        let source = Decoder::new(sound).unwrap();
        sink.append(source);
    }
    else {
        let sound = std::io::Cursor::new(include_bytes!("../sounds/dong.mp3"));
        let source = Decoder::new(sound).unwrap();
        sink.append(source);
    }
    sink.sleep_until_end();
}
