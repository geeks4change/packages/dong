# Dong!

Usage:
```shell
dong [command and parameters]
```
Executes a command, and plays *ding* on success and *dong* on failure.

## Credits
# Ding sound: https://pixabay.com/sound-effects/bell-transition-141421/
# Dong sound: https://pixabay.com/de/sound-effects/failfare-86009/
